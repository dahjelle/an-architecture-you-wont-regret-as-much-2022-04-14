# An Architecture You Won't Regret…as much

**These notes**: https://gitlab.com/dahjelle/an-architecture-you-wont-regret-as-much-2022-04-14/

**Slides**: https://dahjelle.gitlab.io/an-architecture-you-wont-regret-as-much-2022-04-14/

---

> **Blurb**: Software architecture — how all the pieces fit together — has an outsize impact on both the success of a project as well as the happiness of the developers involved. Though I've seen a lot of paradigms for designing architectures over the years, it has seemed that good architecture was as much a matter of intuition rather than science. I've recently learned some vocabulary and techniques (from 1979, cough, cough) that have helped me think much more clearly about the topic and communicate more effectively to others.
>
> **Bio**: I'm a developer at Icon Systems, Inc., where we build software for churches — keeping track of people, donations, and accounting. I went to school for engineering, but kept getting programming jobs, so here I am.

---

## Me

Hi! My name is David Hjelle. I've been at Icon Systems, Inc., for nearly 13 years building church software — keeping track of people, donations, and accounting for churches. 

---

## Architecture

I want to talk a bit about software architecture — how all the bits of an application fit together, how you design and organize your software so that you are happy with it now and in the future. I've recently learned some vocabulary and techniques — going at least as far back as 1979 — that have helped me think much more clearly about the topic.

---

## Where Are We Going?

Before we get started, here's a spoiler: one question and three tools to employ. Don't worry if they don't make sense yet — hopefully they will!

### The Question

What is going to change?

### The Tools

1. modules
2. layers
3. fractals

---

## The Scenario

Okay, let's step back a bit, and talk about the problem we are trying to solve. Does this comic describe any of your software projects?

> For those of you at home, a comic describing the life of a software engineer. On the left, he says, "Clean slate. Solid foundations. *This time* I will build things the *right way*." On the right, an impressive jungle gym of a house, saying, "Oh my. I've done it again, haven't I?"

---

## Conversations

Or, similarly, have you ever had a conversation where you _knew_ your way of organizing your application was better, but you couldn't find a way to successfully communicate it to the rest of your team?

> For those of you at home:
>
> **Calvin**: I need some help with my homework, Hobbes.
>
> **Hobbes**: What's the assignment?
>
> **Calvin**: I'm supposed to write a paper that presents both sides of an issue and then defends on of the arguments.
>
> **Hobbes**: What's your issue?
>
> **Calvin**: That's the problem. I can't think of anything to argue.
>
> **Hobbes**: That's hard to believe.
>
> **Calvin**: I'm always right and everybody else is always wrong! What's to argue about?!

---

## Disclaimer

A quick disclaimer: This is my current thinking on this topic. I don't claim to have it all figured out. I don't even claim to accurately portray the opinions of the people that I'm quoting. I'm really looking forward to your opinions and ideas!

As Fred Brooks, author of *The Mythical Man Month* wrote:

> "[t]here is no single development, in either technology or management technique, which by itself promises even one [order of magnitude](https://en.wikipedia.org/wiki/Order_of_magnitude) improvement within a decade in productivity, in reliability, in simplicity." ~ Fred Brooks in [No Silver Bullet](https://en.wikipedia.org/wiki/No_Silver_Bullet)

---

## An Experiment

Okay, so we've laid the groundwork for what we are covering tonight. Let's dive right in to some specifics. Here's a piece of code for an online store. (This is some strange mashup of PHP, SQL, and HTML.) 

```php
echo db_read("SELECT CONCAT('<html><body>', item, '<a href=/more-info>', price, '</a></body></html>')");
```

If you are not a web programmer, I think this can also make sense in block form.

```mermaid
flowchart LR
    Database --> PHP --> Browser
```

For the most part — don't do this.

> **Side Note:** Oddly enough, this might be a _fantastic_ proof-of-concept. You haven't spent any time architecting, just proving that an idea could work. Whether or not a design is "bad" depends on its purpose.

---

## **ASK**: What problems does this have?

> **Interactivity!**

- Any change to the database requires rewriting your HTML and vice-versa. **(change)**
- Adding an item requires both changing SQL and HTML. **(change)**
- Any output other than HTML would require a completely separate codebase. **(change)**
- Can you imagine trying to add JavaScript for interactivity to that page? **(change)**
- It's a lot of work to get all the quotes correct.
- Mixes up presentation, business logic, and data — why is that a problem?
- Doesn't follow single responsibility principle - why is that important?
- It isn't loosely coupled — what does that mean and why is it important?
- no tests!

> Oddly, it does follow some design principles like [YAGNI](https://en.wikipedia.org/wiki/You_aren't_gonna_need_it) (You Aren’t Gonna Need It), [DRY](https://en.wikipedia.org/wiki/Don't_repeat_yourself) (Don’t Repeat Yourself), and [KISS](https://en.wikipedia.org/wiki/KISS_principle) (Keep It Simple, Stupid).

---

## Improvements

Now, imagine what this might could look like with a better design. If I asked you to improve this design, you might come up with something like this:

```mermaid
flowchart LR
    A[Database] --> B[Data Access]
    B --> C[Business Logic]
    C --> D[API]
    D --> E[Web Client]
```

Some would do it intuitively, some would follow a design pattern like MVC (model-view-controller), some would get there due to experience of things in the past that didn't work. It's not the *only* good design.

More importantly, though, **why** is it a better design? What makes **this** better than **this** (show previous slide)?

---

## David Parnas: "Designing software for ease of extension and contraction" from 1979!

I ran across an academic paper from 1979 — yes, they had computers back then — discussing how to make a piece of software easier to extend and contract, to add stuff to and remove stuff from. (I actually first heard about this Parnas guy from Juval Löwy's book [_Righting Software_](https://rightingsoftware.org). There's a good [Software Engineering Radio](https://www.se-radio.net/2020/04/episode-407-juval-Löwy-on-righting-software/) episode on his ideas.) And even though some of the terminology and constraints are a bit different than ours, I found it a surprisingly easy and insightful read. Parnas addresses this question of good architecture first by turning to engineering.

---

## An all-in-one camera

How would an engineer solve this dilemma of, "I'm going to have to change things in the future?" (This is somewhat more obvious in the physical world with physics as a constraint.)

Let's take a look at cameras. Different lenses are good for different situations. But if you tried to make a camera that had all the lenses…well, it could get interesting. And, the moment you want to change it _further_, you realize that all your efforts to include "all the lenses" failed miserably.

---

## A modular camera

Instead, you:

- figure out what is likely to change between different models or in future versions of the product (in the case of our camera, maybe the lenses, the flash, and the tripod are your anticipated changes),

- pull those characteristics into interchangeable **modules** (we'll define that more in a moment), and,
- make interfaces between those modules that **won't** change.

All of this is guided by our first question: "What is going to change?"

---

## **ASK**: What is going to change?

```php
echo db_read("SELECT CONCAT('<html><body>', item, '<a href=/more-info>', price, '</a></body></html>')");
```

Back to our silly example, and let's apply our question: what is going to change? Many of you have developed software before, and if you haven't you've thought of feature requests for software. What might the owner of this online store want to change?

> **Interactivity!**

- prices and names of products (already handled)
- amount of information per product (title, short description, weight)
- searching and filtering products
- data source (may not be a database, or could be a different database)
- output format (a designer is certain to want different HTML)
- additional items (currently requires changing…even line of code you have :-) )
- maybe you'll need a mobile app or an API
- purchase process (cart + payment processing)
- imagine trying to maintain this?
- mobile app

The answers "What is going to change?" should **guide your design and architecture**. Usually, to figure this out, you need to collaborate between product and engineering. (…which is probably a talk all of its own…)

Again, you want to:

1. figure out what is likely to change,
2. pull those things together into modules,
3. make interfaces between those modules that won't change.

### A Warning

It can be very tempting to ask the question "What is going to change?" and decide that **everything** could possibly change. Well, technically, sure, but that isn't likely or helpful. Your fast-food ordering application won't change into a Tesla control system. You might scale up to Facebook size, but that's not too likely for your gerbil-naming app. Further, some potential changes just aren't a priority for the business — especially given the time or expense they might take. As usual, choosing which changes to account for is one for developers and business folks to decide **together**. Also, Parnas points out that you are not designing something **general** (i.e. that can adapt to all the possibilities without a code change), but something **flexible** (i.e. it can be changed easily to accommodate new requirements)



---

## Modules

So we've talked about the question — "What is going to change?" — and now we are going to get into the three tools. First, **modules**.

I'm not talking JavaScript modules (necessarily) or any specific implementation. You can create a module in a ton of different ways, from a service to a function to an API. The point is collecting things that will change together into a module and exposing a minimal interface to the rest of the system.

What makes for a good module?

- **cohesion** — the stuff inside a module fits well & makes sense together
- **loose coupling** — different modules have minimal connections between each other
- **information hiding** & **encapsulation** — implementation details of a module are hidden behind its interface

Note: these sound like good object-oriented design principles, but 1) quite a few OO programs don't follow this advice and 2) these ideas can be accomplished in any language paradigm.

Fundamentally, these principles are all related to the question of "What is going to change?" — they make change in the future easier to handle.

---

## Layers

So we've talked about the question — "What is likely to change?" — and we talked about the first tool, **modules**. How should your modules be connected to each other? Parnas recommends **layers**. The idea is that each layer has possibly many modules in it. The modules can talk to other modules in that layer, and to modules in the layer below it — and that's it. Layers of abstraction on top of each other, again helping you limit the amount of "damage" any particular change can do.

---

## OSI Layers

Did anyone learn about the layers in the OSI network stack? While practical implementations don't necessarily follow this to a "T", the idea here is exactly what Parnas is talking about. The highest layers require very little, if any, knowledge of the lower layers. I'm pretty glad that I can write software that uses HTTP with very few adjustments necessary for different transport mediums.

Layers like this help limit the scope of changes. Just like **modules**, **layers** help you answer the question "What is going to change?" with an architecture that makes changes easier.

---

## Fractals: Turtles All The Way Down

So we've talked about the question — "What is going to change?" — and we talked about the first tool, **modules**, and the second tool, **layers**.

I'd argue that these tools are useful even down to the design of a set of functions or a class. Like a fractal is self-similar the closer you zoom in, I'd argue that this design works well at every magnification.

---

## Refresher

### The Question

What is going to change?

### The Tools

1. modules
2. layers
3. fractals

---

## Practical Example: When to DRY…or Not

DRY (don't repeat yourself) can be helpful. But "What is going to change?" both explains why and also helps you know when it is not a good idea.

Here's a pair of functions that fetch some JSON data from an API. They look exceptionally similar. A naïve "don't repeat yourself" might think that every repetition of code ought to be done away with.

```javascript
async function orderACar(make, model, color) {
   const response = await fetch('https://orderacar.com/', {
     method: 'POST',
     body: JSON.stringify([
       make,
       model,
       color
     ]),
     headers: {
       'content-type': 'application/json'
     }
   });
   return await response.json();
}

async function orderABurger(meat, weight, toppings) {
   const response = await fetch('https://orderaburger.com/', {
     method: 'POST',
     body: JSON.stringify([
       meat,
       weight,
       toppings
     ]),
     headers: {
       'content-type': 'application/json'
     }
   });
   return await response.json();
}
```

---

## DRY **Without** "What will change?" ☹

If you don't ask, "What is going to change?", you will likely look for **textual** duplication. That can start out just fine…but then turn into a major pain. For instance, imagine that **one** of the two APIs adds a fourth option? Or changes its JSON format? Or starts to use GraphQL requests? Those are changes that you could have _designed_ for.

```javascript
// (probably) DON'T DO THIS!
async function makeOrder(url, option_one, option_two, option_three) {
   const response = await fetch(url, {
     method: 'POST',
     body: JSON.stringify([
       option_one,
       option_two,
       option_three
     ]),
     headers: {
       'content-type': 'application/json'
     }
   });
   return await response.json();
}

async function orderACar(make, model, color) {
  return makeOrder('https://orderacar.com', make, model, color);
}

async function orderABurger(meat, weight, toppings) {
  return makeOrder('https://orderaburger.com', meat, weight, toppings);
}
```

---

## DRY **With** "What is going to change?" ☺

So, just for example, assuming that what will change includes:

1. the URL,
2. the HTTP method,
3. the HTTP headers
4. the body of the request

You can move the details of the _body_ of the request to the calling function, instead of building the body in the common function — that puts the changes where they would actually occur. You've made a **module** to handle HTTP requests, and two other modules to order cars and burgers.

```javascript
// (probably) DO THIS!
async function HTTP({url, method = "POST", headers = {'content-type': 'application/json'}, body}) {
  const response = await fetch(url, {
    method,
    body,
    headers
  });
  return await response.json();
}

async function orderACar(make, model, color) {
  return HTTP({url: 'https://orderacar.com', body: {make, model, color}});
}

async function orderABurger(meat, weight, toppings) {
  return HTTP({url: 'https://orderaburger.com', body: {meat, weight, toppings}});
}
```

---

## Another Example: REST

Take a simple CRUD-style REST API against a database table. `GET`, `POST`, `PUT`, and `DELETE` work great — your client can update the database however it needs. But you may find a problem before too long: if you add a second kind of client and a second database table — all of a sudden the changes you are making need to have matching logic in more than one place. Expressing _action_ verbs — like **markAsCompleted** or **addNewItem** or **updateItem** help keep your client loosely coupled from the data storage mechanisms.

I'm not in the least saying "REST is bad". Rather, if you blindly make something RESTful without considering "What is going to change?", you will likely wish you had done better.

---

## Further Reading

- David Parnas: "[Designing Software for Ease of Extension and Contraction](http://www.pauldee.org/se-must-have/parnas-ease-of-extension.pdf)"
- Juval Löwy: Righting Software
  - https://www.se-radio.net/2020/04/episode-407-juval-Löwy-on-righting-software/ 
  - https://rightingsoftware.org
  - I haven't read all of his book, but it's a good challenge.

---

## Conclusion

### The Question

What is going to change?

### The Tools

1. modules
2. layers
3. fractals

I hope these ideas have given you some food for thought and help you improve your software into something that you don't regret…as much.

---

## Discussion

I'd love to hear your thoughts and questions!

---

# Miscellany

Parnas:

> If [a mechanical or electrical engineer] must develop a family of products, he tries to isolate the changeable parts in modules and to develop an interface between the module and the rest of the product that remains valid for all versions. The crucial steps are:
>
> - Identification of the items that are likely to change.
> - Location of the specialized components in separate modules.
> - Designing intermodule interfaces that are insensitive to the anticipated changes.

> This is a great way of handling slipping schedules: you still end up with releasable software, if with fewer features than originally intended.

> In my experience, identification of the potentially desirable subsets is a demanding intellectual exercise in which one first searches for the _minimal_ subset that might conceivably perform a useful service and then searches for a set of _minimal_ increments to the system.
